#!/usr/bin/env bash

set -Eeuo pipefail

LOCALD_DIR="/etc/rspamd/local.d/"

inotifywait-dir.sh "$LOCALD_DIR" | while read -r line; do
  echo "$line"

  /usr/local/sbin/reload-rspamd.sh
done
