#!/usr/bin/env bash

set -Eeuo pipefail

if (/usr/bin/supervisorctl status rspamd >/dev/null); then
  /usr/bin/supervisorctl signal USR1 rspamd
fi
