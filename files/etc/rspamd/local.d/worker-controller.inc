{% if env.CONTROLLER_PASSWORD %}
bind_socket = "0.0.0.0:11334";
password = "{= env.CONTROLLER_PASSWORD|pbkdf =}";
{% endif %}
