#!/usr/bin/env bash

set -Eeuo pipefail

chown -R rspamd:rspamd /var/lib/rspamd/
chmod -R o+rwX,g+rX,g-w,o-rwx /var/lib/rspamd/
