FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

COPY files/ /

ENV RSPAMD_CONTROLLER_PASSWORD="" \
    RSPAMD_REDIS_SERVERS="" \
    RSPAMD_REDIS_PASSWORD="" \
    RSPAMD_REDIS_HISTORY_NROWS="200" \
    RSPAMD_CLAMAV_SERVERS="" \
    RSPAMD_SCORE_REJECT="" \
    RSPAMD_SCORE_HEADER="" \
    RSPAMD_SCORE_GREYLIST="" \
    RSPAMD_DCC_ENABLED=""

RUN apk add --no-cache \
        rspamd~=3 \
        rspamd-controller~=3 \
        rspamd-proxy~=3 \
        dcc-dccifd~=2 \
        supervisor~=4 \
        inotify-tools~=4 \
        iproute2~=6 \
        bash~=5 \
        run-parts~=4 \
        gettext~=0 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3 \
    && mkdir -p /run/rspamd/

EXPOSE 11332 11333 11334

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
